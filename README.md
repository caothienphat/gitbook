# 80 khái niệm Marketing nhà quản lý cần biết

Các chủ đề từ "Advertising - Quảng cáo" được sắp xếp theo thứ tư chữ cái cho phép người đọc dễ dàng truy cập lời khuyên của ông. Với những từ ngữ thích đáng và rõ ràng, cuốn sách này đủ toàn diện cho các nhà quản lý muốn hoàn tất giai đoạn vỡ lòng về tiếp thị, nhưng cũng là một nguồn tư liệu mới mẻ và tiên tiến nhất cho những nhà tiếp thị đày dan kinh nghiệm muốn theo kịp với tư duy mới nhất.

Bất luận là bạn cần một ý tưởng mới mẻ về cách xây dựng thương hiệu hay những chiến lược mới trong tiếp thị truyền miệng, Thấu hiểu tiếp thị từ A đến Z së trao cho bạn những công cụ bạn cần đẻ cạnh tranh giành khách hàng trên một thị trường đang đổi thay nhanh chóng. Cuốn sách này là một công cụ thiết yếu cho các nhà quản lý, tổng giám đốc, giám đốc tiếp thị, và bất cứ ai muốn hiểu rõ những vấn đề căn bản của tiếp thị.

40 năm trong sự nghiệp tiếp thị đã giúp tôi có được một số kiến thức và một chút uyên thâm. Suy ngẫm về thực trạng của ngành này, tôi cho rằng đây là lúc cần quay lại những khái niệm căn bản của tiếp thị.

Trước tiên, tôi liệt kê ra 80 khái niệm thiết yếu về tiếp thị và dành thời gian để làm sáng tỏ những ý nghĩa và thực tiễn trong kinh doanh. Mục đích chính của tôi là để xác định những nguyên tắc tốt nhất và áp dụng tiếp thị một cách sáng tạo và hiệu quả.

Tôi đã nhận ra rằng chặng đường công việc này chứa đầy ắp những điều ngạc nhiên, mang lại thêm nhiều hiểu biết và triển vọng mới mẻ. Tôi không muốn viết ra thêm 800 trang sách giáo khoa tiếp thị.&#x20;

Và tôi cũng không muốn lặp lại những ý tưởng và những kinh nghiệm mà tôi đã viết ra trong những cuốn sách trước. Tôi chỉ muốn giới thiệu những quan niệm và triển vọng mới mẽ và thú vị để có thể hiểu, làm mẫu, lĩnh hội, và áp dụng bất cứ lúc nào. Vì vậy quyển sách được viết ra dành cho những độc giả sau đây:

* Các nhà quản lý vừa mới hiểu ra rằng họ cần phải biết điều gì đó về tiếp thị. Bạn có thể là phó chủ tịch phụ trách tài chính, hoặc phó giám đốc điều hành của một tổ chức phi lợi nhuận, hoặc là chủ doanh nghiệp chuẩn bị tung ra một sản phẩm mới. Bạn có thể không có thời gian để đọc hết quyển Marketing for Dummies - Tiếp thị cho người mới vào nghề dày 300 trang. Thay vì vậy, bạn muốn hiểu được một số khái niệm chính và nguyên lý tiếp thị được trình bày bằng một ngôn ngữ đích xác một cách tiện lợi.
* Các nhà quản lý đã học một khóa tiếp thị từ vài năm trước và nhận ra rằng mọi thứ nay đã thay đổi. Bạn có thể muốn ôn lại những hiểu biết về các khái niệm tiếp thị cốt yếu và muốn biết thêm những ý tưởng mới nhất để tiếp thị hiệu quả.
* Các chuyên gia tiếp thị đang bối rối trong mớ hỗn độn của những sự kiện tiếp thị diễn ra hàng ngày. Họ muốn lấy lại sự rõ ràng và bổ sung thêm hiểu biết qua việc đọc quyển sách này.

Cách tiếp cận của tôi chịu ảnh hưởng Thiền. Thiền nhấn mạnh việc học bằng cách suy tưởng và trực tiếp từ trực giác nội tại. Những ý tưởng trong cuốn sách này được hình thành từ sự suy tưởng đó của tôi dựa trên những khái niệm và nguyên lý tiếp thị căn bản.

Dù tôi có gọi đó là sự suy tưởng, suy ngẫm, hoặc trầm tư, tôi cũng không cho là tất cả những ý tưởng trong cuốn sách này là của riêng tôi. Một số nhà tư tưởng lớn về kinh doanh và tiếp thị được tôi trích dẫn trực tiếp, hoặc là ảnh hưởng trực tiếp đến những ý tưởng nêu lên ở đây. Tôi đã tiếp thu những ý tưởng của họ thông qua việc đọc, thảo luận, giảng dạy, và tư vấn.

Vấn đề trọng tâm mà các doanh nghiệp hiện nay phải đối mặt không phải là thiếu hàng hóa mà là thiếu khách hàng. Phần lớn các ngành công nghiệp trên thế giới sản xuất ra hàng hóa nhiều hơn số lượng khách hàng có thể tiêu thụ. Sản xuất thặng dư là hậu quả từ những đối thủ cạnh tranh riêng lẻ muốn gia tăng thị phần lớn hơn mức có thể. Nếu mỗi công ty lập kế hoạch tăng doanh số lên 10 phần trăm và toàn bộ thị trưởng tiêu thụ chỉ tăng được 3 phần trăm, thi kết quả là sản xuất thặng dư.

Điều đó dẫn đến một sự cạnh tranh khốc liệt. Những người cạnh tranh nỗ lực tối đa để thu hút khách hàng bằng cách hạ giá bán và thêm tặng phẩm. Những chiến lược này cuối cùng làm giảm thấp tỷ lệ lãi, bớt lợi nhuận dẫn đến thất bại, và có thêm nhiều sự sáp nhập và mua lại công ty.

Tiếp thị là câu trả lời cho làm cách nào cạnh tranh một cách căn bản thay vì giảm giá. Do sản xuất thặng dư nên tiếp thị trở nên quan trọng hơn bao giờ hết. Vì vậy tiếp thị là bộ phận tạo ra khách hàng cho công ty.

Nhưng tiếp thị vẫn còn là một chủ đề bị hiểu rất sai lệch trong phạm vi kinh doanh cũng như trong suy nghĩ của nhiều người. Nhiều công ty cho rằng tiếp thị tổn tại chỉ để giúp khâu sản xuất tống khứ đi sản phẩm. Nhưng sự thật thì ngược lại, khâu sản xuất phải hỗ trợ cho tiếp thị. Mỗi công ty luôn có thể tìm ra cách sản xuất, nhưng điều giúp cho công ty thành đạt chính là những ý tưởng và cách tiếp thị. Sản xuất, tiêu thụ, nghiên cứu và phát triển (R\&D), tài chính, và các chức năng khác của công ty hiện hữu nhằm giúp thực hiện mục tiêu của công việc của công ty là tiếp cận khách hàng.

Tiếp thị thường bị nhầm lẫn với việc bán hàng. Tiếp thị và bán hàng hầu như đổi lập với nhau. "Tiếp thị để cố bán - hard sell marketing" là một việc mâu thuẫn. Từ lâu, tôi đã nói rằng: “Tiếp thị không phải là nghệ thuật nhằm tìm cách tống khứ những gì bạn làm ra thông minh hơn. Tiếp thị là nghệ thuật tạo ra giá trị cho những khách hàng thật sự. Đó là nghệ thuật tạo ra khách hàng thật sự tốt hơn. Khẩu hiệu của người làm tiếp thị là chất lượng, dịch vụ, và giá trị."

Việc bán hàng chỉ khởi sự khi bạn có sản phẩm. Tiếp thị bắt đầu từ trước khi sản phẩm có mặt. Tiếp thị là bài tập mà công ty của bạn cần phải giải đáp khách hàng cần gì và công ty bạn cần phải cung ứng gì. Tiếp thị xác định ra làm thế nào để giới thiệu, định giá, phân phối, và quảng bá sản phẩm và dịch vụ ra thị trường. Sau đó tiếp thị sẽ giảm sát kết quả và dần hoàn thiện sản phẩm. Tiếp thị cũng quyết định khi nào cần chấm dứt việc cung ứng sản phẩm đó.

Như đã nói, tiếp thị không phải là một cố gắng để bán hàng trong ngắn hạn, mà là một nỗ lực đầu tư dài hạn. Tiếp thị cần được thực hiện tốt trước khi công ty cho ra bất kỳ sản phẩm nào hoặc tham gia vào bất kỳ một thị trường nào, và vẫn tiếp tục sau khi đã bán sản phẩm.

[Lester Wunderman](https://en.wikipedia.org/wiki/Lester\_Wunderman), một nhà tiếp thị trực tiếp nổi tiếng, đã nêu rõ sự tương phản giữa bản hàng và tiếp thị như sau: “Bản tụng ca của cuộc Cách mạng Công nghiệp là bản tụng ca của nhà sản xuất, họ nói rằng "Đây là cái do tôi làm ra, sao bạn không vui lòng mua nó đi?" Còn trong thời đại thông tin hiện nay thì lại là người mua hàng đang hỏi: 'Đây là cái tôi muốn, sao bạn không vui lòng sản xuất?"

Tiếp thị hy vọng hiểu rõ khách hàng mục tiêu đến nỗi việc bán hàng không còn cần thiết nữa. [Peter Drucker](https://vi.wikipedia.org/wiki/Peter\_Drucker) khẳng định rằng “mục tiêu của tiếp thị là làm cho việc bán hàng thành vô ích." Tiếp thị chính là khả năng đáp ứng được thị trưởng.

Tuy nhiên cũng có nhiều lãnh đạo doanh nghiệp lại nói rằng: “Chúng tôi không thể mất thời giờ cho việc tiếp thị được. Vì chúng tôi vẫn chưa thiết kế được sản phẩm mà." Hoặc là "Chúng tôi quá thành công rồi thì cần tiếp thị làm gì nữa, và nếu như chúng tôi không thành công thì chúng tôi lại không đủ sức làm việc đó nữa." Tôi nhớ có một lãnh đạo công ty gọi điện thoại nói rằng: "Hãy đến đây ngay và dạy giúp vài bài tiếp thị, doanh thu của tôi vừa bị sụt giảm 30 phần trăm đây này."

Đây là định nghĩa tiếp thị của tôi: Quản trị tiếp thị là nghệ thuật và khoa học lựa chọn các thị trường mục tiêu và tiếp cận, nắm bắt, và tăng thêm khách hàng thông qua việc sáng tạo, thông tin, và cung cấp giá trị ưu việt cho khách hàng.

Hoặc nếu bạn thích một định nghĩa chỉ tiết hơn. "Chức năng của tiếp thị trong kinh doanh là xác định những nhu cầu và ước muốn chưa được đáp ứng, vạch rõ và đo lường quy mô và khả năng sinh lợi tiềm tàng, xác định những thị trường mục tiêu nào mà tổ chức có thể phục vụ tốt nhất, quyết định làm ra các sản phẩm, dịch vụ, và chương trình thích hợp để cung ứng cho những thị trường đã lựa chọn đó, và kêu gọi mọi người trong tổ chức suy nghĩ và phục vụ cho khách hàng đó."

Tóm lại, công việc của tiếp thị là biến đổi những nhu cầu đa dạng của con người thành các cơ hội tạo ra lợi nhuận. Mục tiêu của tiếp thị là tạo ra giá trị bằng cách cung cấp những giải pháp tốt hơn, giúp giảm bớt thời gian và sức lực cho việc tìm mua hàng của khách. Nhờ đó, đưa đến cho toàn xã hội một tiêu chuẩn sống cao hơn.

Thực tiễn tiếp thị ngày nay phải đòi hỏi thoát ra khỏi yêu cầu cứng nhắc phải bán được hàng vốn thường dẫn đến việc bán được hàng trong ngày hôm nay nhưng lại mất khách hàng vào ngày mai. Mục tiêu của tiếp thị là phải xây dựng được mối quan hệ có lợi lâu đài với khách hàng, chứ không nhất thiết chỉ để bán được hàng mà thôi. Không thể xem giá trị của công ty lớn hơn giá trị cung cấp lâu dài cho khách hàng của mình. Vì vậy phải hiểu thật rõ khách hàng của bạn để cung cấp được đúng lúc các hàng hóa, dịch vụ, và những thông điệp thích hợp với nhu cầu cá nhân của họ.

Thông thường tiếp thị được tổ chức thành một phòng trong một doanh nghiệp. Điều này vừa hay lại vừa không hay? Hay vì tập trung được một số nhân sự lành nghề và có khả năng riêng để hiểu, phục vụ và thỏa mãn được nhu cầu khách hàng. Không hay vì lẽ các bộ phận khác nghĩ rằng toàn bộ công việc tiếp thị chỉ do phòng tiếp thị thực hiện thôi. Như David Packard của công ty Hewlett-Packard đã nhận xét: “Tiếp thị quan trọng đến nỗi không thể để riêng cho phòng tiếp thị làm... Trong một công ty thật sự làm tiếp thị tốt, bạn không thể nói được ai là thuộc phòng tiếp thị nữa. Mà tất cả mọi người trong tổ chức đó đều phải quyết định căn cứ trên các tác động đối với khách hàng."

Giáo sư [Philippe Naert](https://en.wikipedia.org/wiki/Philippe\_Naert) cũng có cùng suy nghĩ đó, ông nói: “Bạn không thể có được một văn hóa tiếp thị thật sự bằng cách hấp tấp tạo ra một phòng hay một nhóm tiếp thị, ngay cả khi bạn chỉ định được những nguời thật sự tài năng vào công việc đó. Công việc tiếp thị bắt đầu từ cấp quản lý cao nhất. Nếu người lãnh đạo không hiểu được sự cần thiết phải quan tâm đến khách hàng thì làm sao nhân viên của công ty có thể chấp nhận và thực hiện được ý tưởng tiếp thị?”

Công việc tiếp thị không nên bị bó hẹp trong trong phạm vi một phòng, là nơi làm ra quảng cáo, chọn lựa phương tiện truyền thông, gửi thư trực tiếp, và trả lời những câu hỏi của khách hàng. Tiếp thị là một quy trình rộng lớn hơn, bao gồm việc tính toán một cách hệ thống phải làm ra cái gì, làm cách nào để thu hút sự chú ý và tiếp cận khách hàng, và làm thế nào để khách hàng luôn mong muốn mua nhiều sản phẩm của công ty hơn.

Hơn thế nữa, các chiến lược và hành động tiếp thị không chỉ được thực hiện đối với các khách hàng. Thí dụ như, công ty của bạn muốn tăng thêm vốn từ các nhà đầu tư, bạn phải biết cách tiếp thị nhắm đến những nhà đầu tư. Còn nếu bạn muốn thu hút nhân tài về công ty mình, bạn phải đưa ra được một đề nghị hấp dẫn để có thể lôi kéo được họ. Bất kể là bạn tiếp thị đến khách hàng, hoặc đến nhà đầu tư, hoặc người tài, bạn phải hiểu được những nhu cầu và ước muốn của họ và đưa ra một đề nghị có giá trị cạnh tranh cao để giành được thiện cảm của họ.

Vậy việc học tiếp thị có khó không? Dễ thôi, vì việc học tiếp thị chỉ mất một ngày. Nhưng lại khó, vì có khi phải mất cả một đời để tinh thông nó. Tuy nhiên, dù khó thì cũng cần nhìn vào cái khó đó một cách tích cực. Tôi đồng cảm với nhận xét của [Warren Bennis](https://en.wikipedia.org/wiki/Warren\_Bennis): "Không có gì làm tôi vui hơn là được học thêm điều mới mẻ.” (Ông Bennis là Giáo sư Danh dự của trường Đại học California và là một tác giả xuất chúng về đề tài lãnh đạo).

Tin tốt đối với bạn là tiếp thị sẽ mãi mãi cần thiết. Còn tin xấu, nó sẽ không còn giống như cách bạn đã từng học. Trong thập kỷ tới, tiếp thị sẽ thay đổi từ A tới Z. Tôi đã chọn lọc để nêu lên 80 trong số những khái niệm và ý tưởng tiếp thị then chốt nhất dành cho doanh nhân, họ cần để tham chiến trên một thị trường vừa cạnh tranh khốc liệt và vừa thay đổi nhanh chóng tùng ngày.

