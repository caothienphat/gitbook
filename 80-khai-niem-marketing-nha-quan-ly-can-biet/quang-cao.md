# Quảng cáo

Tôi và cũng như phần lớn các bạn vừa thích vừa ghét trong quan hệ với quảng cáo. Đúng như vậy, tôi ưa thích từng mẩu quảng cáo rượu vốt ka Absolut mới: Để coi họ sẽ giấu chai rượu nổi tiếng này ở đâu? Tôi cũng thích cách quảng cáo trào lộng Kiều Anh, cũng như tính mạo hiểm kiểu Pháp. Tôi cũng nhớ rõ cả những mẩu quảng cáo nghe vui tai và giàu âm điệu. Tuy nhiên tôi không thích phần lớn mẫu quảng cáo. Thực tế là tôi cố tình lờ chúng đi. Chúng làm gián đoạn dòng suy nghĩ của tôi. Nhiều mấu còn tệ hơn: chúng làm tôi phát cáu.

### Những quảng cáo hay nhất không phải chỉ ở sự sáng tao, mà có thể bán được hàng

Sáng tạo không thôi thì không đủ. Quảng cáo phải hơn một hình thức nghệ thuật. Nhưng nghệ thuật giúp cho quảng cáo. [William Bernbach](https://en.wikipedia.org/wiki/William\_Bernbach), cựu lãnh đạo công ty Doyle, Dane & Bernbach đã nhận xét: “Những sự kiện thôi thì chưa đủ... Đừng quên rằng Shakespeare đã dùng những cốt chuyện khá nhàm, nhưng thông điệp của ông đã được truyền đi với sức mê hoặc lớn lao.”

Ngay cả những quảng cáo rất thành công cũng cần phải đổi mới nếu không muốn bị lạc hậu. Coca-Cola không thể tiếp tục mãi một câu quảng cáo như “Đây là Thứ Thật," "Coke là vậy đó” hoặc “Tôi muốn dạy cho thế giới hát.” Quảng cáo sẽ mờ nhạt dần đi là điều hiển nhiên.

Những nhà quảng cáo hàng đầu khác nhau về cách tạo ra một chương trình quảng cáo hiệu quả. [Rosser Reeves của công ty quảng cáo Ted Bates](https://en.wikipedia.org/wiki/Rosser\_Reeves) thích liên kết tên nhãn hiệu với một tiện ích riêng biệt, thí dụ “R-O-L-A-I-D-S được đánh vần là RELIEF" nghĩa là (sự khuây khỏa). Leo Burnett thì ưa chuộng tạo ra một nét đặc sắc thể hiện tiện ích hoặc tính cách của sản phẩm, như Người Khổng Lồ Xanh, Người Lính Pillsbury, chàng cao bồi Marlboro, và nhiều nhân vật thần thoại khác. Hãng Doyle, Dane & Bernbach thì lại thích phát triển một kịch bản với tình tiết xoay quanh một vấn đề và kết cục của nó: theo cách đó, quảng cáo của Federal Express chiếu cảnh một người đã lo lắng về việc nhận một món hàng vào thời gian đã hẹn trước, sau đó thì yên tâm nhờ dùng hệ thống theo dõi của FedEx.

### Mục tiêu của quảng cáo không phải chỉ để đưa ra những thông tin về sản phẩm, mà còn để bán ra một giải pháp hay một điều mơ ước

Hãy hướng quảng cáo của bạn đến những cảm xúc của khách hàng. Đó là điều mà Ferrari, Tiffany, Gucci, và Ferragamo làm. Hãng ô-tô Ferrari đem lại ba giấc mơ: xã hội công nhận, sự tự do, và chủ nghĩa anh hùng. Hãy nhớ rằng [Charles Revson](https://en.wikipedia.org/wiki/Charles\_Revson), người sáng lập công ty Revlon nhận xét: "Trong nhà máy, chúng tôi sản xuất son môi. Còn trong quảng cáo, chúng tôi bán ra hy vọng."

Nhưng hứa hẹn về những giấc mơ chỉ làm người ta nghi ngờ quảng cáo. Họ không tin rằng việc lựa chọn một chiếc xe hay một loại nước hoa nào đó làm họ trở nên hấp dẫn hơn hoặc được chú ý hơn. Stephen Leacock, một học giả và là một người trào phúng, đã giễu cợt quảng cáo như sau: “Quảng vào có thể được mô tả là một khoa học nhằm nắm lấy trí tuệ của con người cho đến lúc moi được tiền ra.”

### Quảng cáo chủ yếu tạo ra sự chú ý, đôi khi là sự hiểu biết về sản phẩm, thỉnh thoảng là sự ưa thích sản phẩm, và hiểm khi tạo ra việc mua hàng

Đó là lý do mà quảng cáo không thể được thực hiện riêng lẻ. Cần phải có thêm hoạt động xúc tiến bán hàng để thúc đẩy việc mua hàng. Nhân viên bán hàng có khi phải giải thích thêm các tiện ích và cũng khá vất vả để có thể bán được hàng.

Điều đáng buồn là rất nhiều quảng cáo chẳng có sáng tạo gì cho lắm. Phần lớn không có gì đáng để nhớ. Thí dụ một số quảng cáo về xe ô-tô. Một mẩu quảng cáo tiêu biểu chiếu cảnh một kiểu xe mới chạy với tốc độ 100 dặm một giờ quanh một sườn núi. Nhưng ở Chicago chúng tôi lại không hề có núi. Và tốc độ cho phép thì chỉ được 60 dặm một giờ. Còn hơn thế nữa, tôi không thể nào nhớ rõ mẫu quảng cáo này đã thể hiện loại xe nào. Kết luận: phần lớn quảng cáo của các công ty chỉ là lãng phí tiền bạc của họ cũng như thời gian của tôi mà thôi.

Nhiều công ty quảng cáo đổ lỗi sự thiếu sáng tạo đó là do khách hàng. Nhiều khách hàng khôn ngoan yêu cầu công ty quảng cáo xây dựng ba mẫu, từ nhẹ nhàng cho tới phóng túng. Nhưng thông thường sau đó, người khách này thường chọn lấy mẩu nhẹ nhàng và an toàn nhất. Và như thế họ đã đóng vai trò giết chết đi một mẫu quảng cáo hay.

Các công ty nên đặt ra câu hỏi sau đây trước khi sử dụng quảng cáo: Liệu quảng cáo có tạo ra nhiều khách hàng hài lòng hơn so với việc công ty chúng ta chi ra cùng món tiền đó để sản xuất ra một sản phẩm tốt hơn, cải thiện dịch vụ công ty, hoặc tạo ra ấn tượng hiệu quả hơn cho thương hiệu? Tôi mong rằng các công ty sẽ dùng nhiều tiền của và thời gian hơn để tạo ra những sản phẩm thượng hạng, và bớt cố gắng nhồi nắn nhận thức của khách hàng qua các chiến dịch quảng cáo tốn kém. Sản phẩm càng tốt thì càng ít phải tốn tiền cho quảng cáo cho nó. Cách quảng cáo tốt nhất là từ những khách hàng hài lòng của bạn.

Khách hàng càng trung thành gắn bó thì bạn càng tiết kiệm được tiền quảng cáo. Trước hết, hầu hết khách hàng sẽ quay trở lại mà không cần bạn quảng cáo gì cả. Kế đến, nhiều khách hàng vì rất hài lòng sẽ quảng cáo thay cho bạn. Hơn thế nữa, quảng cáo thường chỉ hấp dẫn được những khách hàng thích so bì giá cả, họ chỉ vụt đến rồi đi để lùng kiếm hàng rẻ.

Có rất nhiều người thích quảng cáo cho dù có hiệu quả hay không. Tôi không muốn ám chỉ đến những người xem chương trình nhiều tập trên tivi chỉ cần có quảng cáo thương mại giữa chừng để tranh thủ đi vệ sinh. Một người bạn cũ thông thái của tôi, Tiến sĩ Steuart Henderson Britt, đã tin tưởng quảng cáo một cách nhiệt thành như sau: "Làm kinh doanh mà không có quảng cáo thì cũng giống như nháy mắt tỏ tình với một cô gái trong bóng đêm. Bạn biết mình đang làm gì, nhưng chẳng ai khác biết cả.”

Câu thần chú của các công ty quảng cáo là: “Đi ngủ sớm, thức dậy sớm, làm việc như điên, quảng cáo."

Còn tôi vẫn cứ khuyên là: “Hãy quảng cáo cho hay, đừng quảng cáo dở.” David Ogilvy cảnh báo rằng: "Đùng bao giờ viết ra một mẫu quảng cáo mà bạn không muốn người trong gia đình bạn đọc. Bạn không nói dối vợ mình. Vậy cũng đừng nói điều đó với tôi.”

Ogilvy đã chỉ trích những người quảng cáo chỉ lo tìm kiếm giải thưởng, chứ không phải để bán hàng rằng: “Ngành quảng cáo... đang bị trì kéo xuống bởi những người tạo ra nó. Họ chẳng biết bán gì cả và chẳng hề bán được một chút gì trong suốt cuộc đời họ... họ xem thường việc bán hàng. Cuộc đời họ chỉ để phô trương sự thông minh, và lừa bịp khách hàng đưa tiền để họ phô bày sự độc đáo và tài năng của họ thôi.”

Còn những người ưa chuộng quảng cáo có thể dẫn ra nhiều trường hợp có kết quả xuất sắc như: thuốc lá Marlboro, rượu vốt ka Absolut, xe hoi Volvo. Những trường hợp sau đây cũng có tác dụng:

* Một công ty quảng cáo tìm người bảo vệ. Ngay hôm sau, công ty đó bị cướp.
* Nếu bạn nghĩ rằng quảng cáo không đáng đồng tiền – Chúng ta biết rằng có 25 ngọn núi ở Colorado cao hơn ngọn Pike (ở miền Bắc nước Anh). Vậy bạn có thể kể tên một trong số những ngọn núi đó không?

Những người quyết chống lại sự lệ thuộc vào quảng cáo rất thích trích dẫn câu của John Wanamaker, chủ một cửa hàng nổi danh: “Tôi biết rằng phân nửa số tiền tôi chi cho quảng cáo là lãng phí, nhưng tôi chẳng bao giờ biết được đó là phân nửa nào?”

### Vậy bạn nên làm quảng cáo như thế nào?

Bạn phải quyết định dựa trên 5 chữ M của quảng cáo, đó là: mission - nhiệm vụ, message - thông điệp, media - phương tiện truyền thông, money - tiền, và measurement - đánh giá kết quả.

Quảng cáo có thể có một trong số bốn nhiệm vụ sau đây: để thông tin, để thuyết phục, để nhắc nhớ, hoặc để củng cố thêm quyết định mua hàng. Với một sản phẩm mới, bạn nên thông tin hoặc/và thuyết phục. Còn với một sản phẩm cũ như Coca-Cola chẳng hạn, bạn nên nhắc nhớ khách hàng. Với sản phẩm vừa mới được mua, bạn nên trấn an khách hàng và củng cố quyết định của họ.

Thông điệp phải truyền đạt được giá trị đặc biệt của hàng hóa bằng lời lẽ và hình ảnh. Mọi thông điệp nên được thử nghiệm trước với khách hàng mục tiêu bằng cách sử dụng một nhóm sáu câu hỏi sau đây:

1. Thông điệp chính mà bạn nhận được từ quảng cáo này là gì?
2. Bạn nghĩ là người quảng cáo muốn bạn biết gì, tin điều gì, hoặc làm gì?
3. Quảng cáo này tác động bạn đến mức nào để bạn làm theo điều nó gợi ý?
4. Quảng cáo này có phần nào hữu hiệu và phần nào dở?
5. Quảng cáo này cho bạn cảm nhận gì?
6. Nơi nào là lý tưởng nhất để thông điệp này đến được với bạn – Ở đâu mà bạn có thể nhận thấy và chú ý đến nó dễ nhất?

Phương tiện truyền thông phải được chọn lựa dựa trên khả năng vươn tới thị trường mục tiêu một cách hiệu quả. Bên cạnh những phương tiện truyền thống như báo chí, tạp chí, truyền thanh, truyền hình, và các bảng hiệu, còn có hàng loạt những phương tiện mới như thư điện tử, fax, tiếp thị qua điện thoại, tạp chí điện tử, bảng quảng cáo trong cửa hàng, quảng cáo hiện ra trong thang máy và nhà vệ sinh các nhà cao tầng. Việc chọn lựa phương tiện nào để quảng cáo đang trở thành một thách thức lớn.

Công ty nên cùng làm việc với bộ phận phương tiện truyền thông của hãng quảng cáo để nhận định xem độ tiếp cận, tần suất, và tác động của chiến dịch quảng cáo sẽ đạt tới mức độ nào. Thí dụ như bạn muốn chiến dịch quảng cáo được xuất hiện ít nhất là một lần cho 60 phần trăm của thị trường mục tiêu gồm có một triệu người. Như vậy cần 600.000 lần xuất hiện. Nhưng bạn còn muốn mỗi người cần thấy được quảng cáo này bình quân ba lần trong cả chiến dịch. Vậy quảng cáo này phải có 1.800.000 lần xuất hiện. Nhưng để mỗi người chú ý đến quảng cáo ba lần như vậy thì phải có sáu lần xuất hiện. Cuối cùng bạn phải tính ra là cần 3.600.000 lần xuất hiện. Và nếu bạn muốn dùng một phương tiện truyền thông danh tiếng với giá 20 đô-la cho 1.000 lần xuất hiện thì chiến dịch sẽ tốn 72.000 đô-la (20 x 3.600.000/1.000). Cần lưu ý là công ty bạn có thể dùng cùng số tiền đó để đưa quảng cáo đến được nhiều người hơn với tần suất xuất hiện ít hơn; hoặc đến nhiều người hơn với phương tiện truyền thông rẻ hơn. Đây là việc tính toán cân đối giữa phạm vi tiếp cận, tần suất xuất hiện, và tác động của quảng cáo.

Kế đến là tiền. Ngân sách dành cho quảng cáo được tính ra từ những quyết định mức tiếp cận, tần suất xuất hiện, và tác động với mức giá nào. Ngân sách này phải tính đến việc công ty phải trả tiền quảng cáo và các chi phí khác.

Một xu hướng được hoan nghênh là người quảng cáo sẽ trả cho các hãng quảng cáo dựa trên kết quả. Điều đó sẽ hợp lý hơn vì các hãng quảng cáo thường cho rằng chương trình quảng cáo sáng tạo của họ giúp tăng doanh thu của công ty. Cách làm mới này sẽ trả cho đại lý quảng cáo 18 phần trăm hoa hồng nếu doanh thu có tăng, trả mức thông thường 15 phần trăm nếu doanh thu vẫn giữ nguyên, và chỉ trả 13 phần trăm kèm theo lời cảnh báo nếu doanh thu sụt giảm. Tất nhiên lúc đó hãng quảng cáo sẽ cho rằng doanh thu sút giảm là do những tác động khác, ngay cả khi giảm rất nhiều thì cũng chẳng phải tại vì quảng cáo.

### Phần đánh giá, đo lường hiệu quả quảng cáo

Các chiến dịch quảng cáo cần phải có cách đánh giá hiệu quả trước và sau đó. Mô hình quảng cáo nên được thử nghiệm trước để biết được hiệu quả truyền đạt bằng những cách thức gợi nhớ, nhận ra, và thuyết phục. Khi đánh giá hậu kỳ phải tính ra cho được tác động của thông tin truyền đạt hoặc của việc bán hàng do chiến dịch quảng cáo đem lại. Dù làm điều này không dễ, nhất là khi quảng cáo bằng hình ảnh.

Thí dụ, làm thế nào Coca-Cola có thể đánh giá được tác động của bức hình chai Coke trên bìa sau của một tờ tạp chí mà công ty đã phải chi tới 70.000 đô-la để tác động đến việc mua hàng? Với giá 70 cent và lãi 10 cent một chai, Coke phải bán thêm 700.000 chai để bù đắp phí tổn 70.000 đô-la cho mẫu quảng cáo đó. Tôi không thể nào tin được là quảng cáo như vậy sẽ giúp Coke bán thêm được 700.000 chai.

Tất nhiên, công ty phải cố gắng đánh giá được kết quả của mỗi phương tiện và cách thức quảng cáo. Nếu cách quảng cáo trực tuyến đang thu hút được nhiều khách hàng tiềm năng hơn là truyền hình thì hãy điều chỉnh lại ngân sách để làm theo cách thứ nhất. Đừng duy trì một ngân sách quảng cáo cố định mà hãy phân bổ vào phương tiện nào tạo ra được sự hưởng ứng của thị trường nhiều nhất.

Một điều chắc chắn là chi tiền quảng cáo cho những sản phẩm kém phẩm chất hoặc không có gì đặc sắc sẽ dẫn đến lãng phí. Như Pepsi Cola đã chi tới 100 triệu đô-la để tung ra sản phẩm Pepsi One, và đã thất bại. Thực tế là quảng cáo cho một sản phẩm tồi là cách hay nhất để tiêu diệt nó. Nhiều người sẽ thử nó và nhanh chóng nói cho người khác biết nó dở hoặc chẳng đáng đến cỡ nào.

### Bạn nên chỉ cho quảng cáo bao nhiêu?

Nếu bạn chỉ quá ít, có nghĩa là bạn đang chi quá nhiều vì chẳng làm ai quan tâm. Chỉ một triệu đô-la quảng cáo trên tivi chưa chắc được chú ý. Và nếu bạn chỉ nhiều triệu đô-la hơn, lợi nhuận của bạn sẽ bị tổn thất. Nhiều công ty quảng cáo thúc ép bạn chỉ ra một ngân sách khổng lồ và dù có tạo ra được sự chú ý thì nó cũng chẳng đẩy doanh số lên được bao nhiêu.

Khó mà đo lường được cái không thể đo lường. Stan Rapp và Thomas Collins đã chỉ rõ vấn đề này trong cuốn sách Beyond MaxiMarketing - Đằng sau Tiếp Thị Cực Đại. “Chúng tôi chỉ muốn nhấn mạnh rằng việc nghiên cứu thường đi quá xa để đo lường những điều không hề có liên quan, như ý kiến của con người hoặc trí nhớ của họ về quảng cáo, thay vì đo lường hành động của họ do việc quảng cáo tạo ra."

Vậy thì ảnh hưởng và sử dụng quảng cáo đại trà sẽ bớt đi không? Tôi cho là vậy. Người ta ngày càng chỉ trích và chẳng màng đến quảng cáo nữa. Một trong số nhiều người đã chi tiền rất nhiều cho quảng cáo, Sergio Zyman, cựu Chủ tịch Coca- Cola gần đây đã nói: “Quảng cáo, như bạn biết, đã chết rồi.” Rồi ông đưa ra một định nghĩa quảng cáo mới như sau: “Quảng cáo bao hàm nhiều hơn chứ không phải chỉ là với những quảng cáo trên truyền hình - đó là quảng bá thương hiệu, bao bì, dùng những người phát ngôn nổi tiếng, tài trợ, thông tin, dịch vụ khách hàng, ngay cả cách bạn đối xử với nhân viên, và cách người thư ký của bạn trả lời điện thoại nữa.” Đó là cách ông ta định nghĩa thật sự về tiếp thị.

Điểm hạn chế chính của quảng cáo là nó độc thoại. Bằng chứng là phần lớn các quảng cáo không có số điện thoại hoặc địa chỉ email để khách hàng có thể phản ánh. Thật đáng tiếc cho cơ hội mà công ty đã mất đi trong việc học hỏi được điều gì đó từ khách hàng. Nhà tư vấn tiếp thị Regis McKenna đã nhận xét: “Chúng ta đang chứng kiến sự lỗi thời của quảng cáo. Tiếp thị kiểu mới đòi hỏi một hệ thống thông tin phản hồi, đó chính là yếu tố thiếu sót của quảng cáo mang tính độc thoại.”



